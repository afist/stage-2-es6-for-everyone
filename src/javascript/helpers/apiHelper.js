import {createFightLog} from "../form";

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';

const FIGHTERS_AMOUNT = 2;

function callApi(endpoind, method) {
    const url = API_URL + endpoind;
    const options = {
        method
    };

    return fetch(url, options)
        .then(response =>
            response.ok ? response.json() : Promise.reject(Error('Failed to load'))
        )
        .catch(error => {
            throw error;
        });
}

const random = () => Math.round(Math.random() * 2 + 1);

function fight(firstFighter, secondFighter) {
    const hitPower = firstFighter.getHitPower();
    const blockPower = secondFighter.getBlockPower();
    let damage = 0;
    let healthLeft = secondFighter.getHealth();
    if (hitPower > blockPower) {
        damage = hitPower - blockPower;
         healthLeft = secondFighter.getHealth() - damage > 0
            ? secondFighter.getHealth() - damage
            : 0;
    }
    console.log(`${firstFighter.name} ударил на ${hitPower}, ${secondFighter.name} поставил защиту на ${blockPower}, прошел урон на ${damage}`);


    if (healthLeft === 0) {
        alert(`Winner ${firstFighter.name}, congratulations!!!`);
        window.location.reload();
    } else {
        secondFighter.setHealth(healthLeft);
        return healthLeft;
    }

}


export {callApi, random, fight, FIGHTERS_AMOUNT}