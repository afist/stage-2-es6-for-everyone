import View from './view';
import FighterView from './fighterView';
import {fighterService} from "./services/fightersService";
import {fight, FIGHTERS_AMOUNT} from "./helpers/apiHelper";
import Form from "./form";
import Fighter from "./fighter";


class FightersView extends View {
    constructor(fighters) {
        super();
        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters, this.handleFighterClick);
        this.createFighters = this.createFighters.bind(this);
        this.addFighterCharacteristic = this.addFighterCharacteristic.bind(this);
    }

    fightersMap = new Map();
    fightersDetailsMap = new Map();


    createFighters(fighters) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement({tagName: 'div', className: 'fighters'});
        this.element.append(...fighterElements);
    }

    async handleFighterClick(event, fighter) {
        this.fightersDetailsMap.set(fighter._id, fighter);
        if (!this.fightersDetailsMap.has(fighter._id)) {
            const fighterDetails = await fighterService.getFighterDetails(
                fighter._id
            );
            this.fightersDetailsMap.set(fighter._id, fighterDetails);
        }
        if (this.fightersMap.size == FIGHTERS_AMOUNT) {
            const secondFighterId = Array.from(this.fightersMap.keys()).filter(
                id => +id !== +fighter._id
            )[0];
            if (this.fightersMap.has(fighter._id)) {
                fight(this.fightersMap.get(fighter._id), this.fightersMap.get(secondFighterId));
            }
        } else {
            console.log(this.fightersDetailsMap.get(fighter._id));
            const modal = new Form(
                this.fightersDetailsMap.get(fighter._id),
                this.addFighterCharacteristic
            );
            this.element.appendChild(modal.element);
        }
    }

    addFighterCharacteristic(event, fighter) {
        this.fightersDetailsMap.set(fighter._id, fighter);
        this.fighter = new Fighter(fighter.name, fighter.health, fighter.attack, fighter.defense);
        this.fightersMap.set(fighter._id, this.fighter);

        if (this.fightersMap.size === FIGHTERS_AMOUNT) {
            const fighterElements = this.element.querySelectorAll(".fighter");
            if (fighterElements.length > FIGHTERS_AMOUNT) {
                const fightersArray = Array.from(this.fightersMap.keys());
                const [firstFighterId, secondFighterId] = fightersArray;
                fighterElements.forEach(item => {
                    if (+item.id !== +firstFighterId && +item.id !== +secondFighterId) {
                        item.classList.add("hidden");
                    }
                });
                if (!this.element.querySelector(".rotate")) {
                    firstFighterId > secondFighterId
                        ? this.element
                            .querySelector(`[id="${firstFighterId}"] .fighter-image`)
                            .classList.add("rotate")
                        : this.element
                            .querySelector(`[id="${secondFighterId}"] .fighter-image`)
                            .classList.add("rotate");
                }
            }
        }
    }
}

export default FightersView;